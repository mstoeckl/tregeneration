#include <Python.h>
#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <cstdio>
#include <unordered_map>
#include <map>
#include <tuple>

using std::string;
using std::vector;
using std::shared_ptr;
using std::unordered_map;
using std::map;
using std::tuple;
using std::istream;

static unordered_map<string, long> unicounts;

static string castToString(PyObject* wannabestring, bool* ok)
{
    /* Load either ascii string or binary string (for convienience) */
    if (PyUnicode_KIND(wannabestring) == PyUnicode_1BYTE_KIND) {
        *ok = true;
        return string((char*)PyUnicode_DATA(wannabestring));
    }
    else if (PyBytes_Check(wannabestring)) {
        *ok = true;
        return string(PyBytes_AsString(wannabestring));
    }
    else {
        *ok = false;
        return string();
    }
}

/* copied from http://stackoverflow.com/questions/478898/how-to-execute-a-command-and-get-output-of-command-within-c */
static string exec(const string& cmd)
{
    shared_ptr<FILE> pipe(popen(cmd.data(), "r"), pclose);
    if (!pipe)
        return "ERROR";
    char buffer[128];
    string result = "";
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

static PyObject* ngrams_query_count(PyObject* self, PyObject* args)
{
    bool ok;
    string v = castToString(args, &ok);
    if (!ok) {
        PyErr_SetString(PyExc_RuntimeError, "Expected a pure ascii string.");
        return NULL;
    }

    auto cc = unicounts.find(v);
    if (cc == unicounts.end()) {
        return PyLong_FromLong(0);
    }
    else {
        return PyLong_FromLong(cc->second);
    }
}

#define TAIL_ID 100
#define HEAD_ID 99

#define PUNCT_ID 10
#define ADJ_ID 11
#define ADP_ID 12
#define ADV_ID 13
#define CONJ_ID 14
#define DET_ID 15
#define NOUN_ID 16
#define NUM_ID 17
#define PRON_ID 18
#define PRT_ID 19
#define VERB_ID 20
#define X_ID 21

struct {
    const char* desc;
    int key;
    bool pos;
} SPECIAL_TYPES[] = {
    /* parts of speech */
    { ".", PUNCT_ID, true },
    { "ADJ", ADJ_ID, true },
    { "ADP", ADP_ID, true },
    { "ADV", ADV_ID, true },
    { "CONJ", CONJ_ID, true },
    { "DET", DET_ID, true },
    { "NOUN", NOUN_ID, true },
    { "NUM", NUM_ID, true },
    { "PRON", PRON_ID, true },
    { "PRT", PRT_ID, true },
    { "VERB", VERB_ID, true },
    { "X", X_ID, true },
    /* start/end markers */
    { "HEAD", HEAD_ID, false },
    { "TAIL", TAIL_ID, false },
    /* end of list */
    { nullptr, -1, false }

};
#define N_RESERVED_IDS 256

static PyObject* ngrams_list_pos_categories(PyObject* self, PyObject* args)
{
    PyObject* obj = PyList_New(12);
    int k = 0;
    for (int i = 0; SPECIAL_TYPES[i].desc; i++) {
        if (SPECIAL_TYPES[i].pos) {
            PyList_SetItem(obj, k, PyUnicode_FromString(SPECIAL_TYPES[i].desc));
            k++;
        }
    }
    return obj;
}

static long load_pos_table(string path)
{
    /* Big caveat: Yikes is this lookup table
     * method inefficient! */
    // TODO: fork zcat and process results simultaneously (loads faster, uses less memory)
    string data = exec("zcat " + path);
    size_t start = 0;
    size_t len = data.size();
    bool first = true;
    long ccount = 0;
    printf("Loaded file: %s. Processing...\n", path.data());
    while (true) {
        /* char is unsigned */
        while (start < len && (data[start] == '\n' || data[start] == '\t')) {
            start++;
        }
        size_t end = start;
        while (end < len && (data[end] != '\n' && data[end] != '\t')) {
            end++;
        }
        if (end >= len) {
            break;
        }
        string obj = data.substr(start, end - start);
        if (first) {
            /* a new value */
            ccount = std::stol(obj);
        }
        else {
            /* commit count */
            unicounts[obj] = ccount;
        }
        /* next obj */
        first = !first;
        start = end;

        /* check for signals every 20K words */
        if (unicounts.size() % 20000 == 0) {
            int res = PyErr_CheckSignals();
            if (res) {
                printf("Cancelled load due to interrupt!\n");
                return -1;
            }
        }
    }
    printf("Have loaded %ld word-tag units and frequencies\n", unicounts.size());
    return unicounts.size();
}

static PyObject* ngrams_load_pos(PyObject* self, PyObject* args)
{
    bool ok;
    string path = castToString(args, &ok);
    if (!ok) {
        PyErr_SetString(PyExc_RuntimeError, "Expected a pure ascii string as the file path.");
        return NULL;
    }

    if (unicounts.size() > 0) {
        return PyLong_FromLong(unicounts.size());
    }

    long counts = load_pos_table(path);
    if (counts < 0)
        return nullptr;

    return PyLong_FromLong(counts);
}

static PyObject* ngrams_random(PyObject* self, PyObject* args)
{
    static bool seeded = false;
    if (!seeded) {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC, &t);
        srand(t.tv_nsec);
        seeded = true;
    }
    double d = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    return PyFloat_FromDouble(d);
}

static PyObject* ngrams_rate(PyObject* self, PyObject* args)
{
    int s = PyList_Check(args);
    if (s) {
        Py_ssize_t size = PyList_Size(args);
        for (Py_ssize_t i = 0; i < size; i++) {
            /* borrowed reference, no issues */
            bool ok;
            string str = castToString(PyList_GetItem(args, i), &ok);
            if (!ok) {
                goto err;
            }
            // Do something with the string!
            (void)str;
        }

        /* Call actual rating function here, with std::string array?  */

        double d = 1.5 * size;
        return PyFloat_FromDouble(d);
    }

err:
    PyErr_SetString(PyExc_RuntimeError, "Expected a list of ascii strings as the sole argument.");
    return NULL;
}

static PyObject* ngrams_system(PyObject* self, PyObject* args)
{
    const char* command;
    int sts;

    if (!PyArg_ParseTuple(args, "s", &command))
        return NULL;
    sts = system(command);
    return PyLong_FromLong(sts);
}

struct NGramHash {
    // Hash functions from markoshuffle
    size_t operator()(const tuple<int>& k) const
    {
        return std::get<0>(k) * 1012597;
    }
    size_t operator()(const tuple<int, int>& k) const
    {
        return std::get<0>(k) * 1012597 + std::get<1>(k) * 1224131;
    }
    size_t operator()(const tuple<int, int, int>& k) const
    {
        return std::get<0>(k) * 1012597 + std::get<1>(k) * 1224131
            + std::get<2>(k) * 927779;
    }
    size_t operator()(const tuple<int, int, int, int>& k) const
    {
        return std::get<0>(k) * 1012597 + std::get<1>(k) * 1224131
            + std::get<2>(k) * 927779 + std::get<3>(k) * 495307;
    }
};

static unordered_map<string, int> idLookup;
static unordered_map<std::tuple<int>, long, NGramHash> unigrams;
static unordered_map<std::tuple<int, int>, long, NGramHash> bigrams;
static unordered_map<std::tuple<int, int, int>, long, NGramHash> trigrams;
static unordered_map<std::tuple<int, int, int, int>, long, NGramHash> quatrigrams;

static bool load_ngram_table(string path)
{
    // Translated from markoshuffle, readTSVData.
    // It is very important that we have UNIX line endings.
    string data = exec("zcat " + path);
    printf("Loaded file: %s. Processing...\n", path.data());
    size_t len = data.size();
    size_t eol = 0;
    size_t markers[6];
    size_t codes[6];
    size_t nrows = 0;
    while (true) {
        if (eol >= len) {
            return true;
        }
        size_t sol = eol;
        markers[0] = sol;
        int fnd = 1;
        while (data[eol] != '\n' && eol < len) {
            if (data[eol] == '\t') {
                markers[fnd] = eol;
                fnd++;
            }
            eol++;
        }
        markers[fnd] = eol;
        fnd++;
        eol++;
        nrows++;

        string qq = data.substr(markers[0], markers[1] - markers[0]);
        long count = std::stol(qq);
        for (int i = 1; i < fnd - 1; i++) {
            string wrd = data.substr(markers[i] + 1, markers[i + 1] - markers[i] - 1);
            auto cc = idLookup.find(wrd);
            int id;
            if (cc == idLookup.end()) {
                id = idLookup.size() + N_RESERVED_IDS;
                idLookup[wrd] = id;
            }
            else {
                id = cc->second;
            }
            codes[i - 1] = id;
        }
        switch (fnd - 2) {
        case 1:
            unigrams[tuple<int>(codes[0])] = count;
            break;
        case 2:
            if (codes[1] == 285023 && 45461 == codes[0]) {
                printf("quick brown !!\n");
            }
            bigrams[tuple<int, int>(codes[0], codes[1])] = count;
            break;
        case 3:
            trigrams[tuple<int, int, int>(codes[0], codes[1], codes[2])] = count;
            break;
        case 4:
            quatrigrams[tuple<int, int, int, int>(codes[0], codes[1], codes[2], codes[3])] = count;
            break;
        default:
            // Silently ignore?
            printf("Oops %d\n", fnd);
            break;
        }

        /* check for signals every 20K rows */
        if (nrows % 20000 == 0) {
            int res = PyErr_CheckSignals();
            if (res) {
                printf("Cancelled load due to interrupt!\n");
                return false;
            }
        }
    }
}

static bool load_tagged_corpus(string path, size_t limit)
{
    // Read a corpus, simulatenously translating its tags down to a proper subset,
    // loading the retagged tokens in the POS DB, and updating ngram
    // counts with all 2^k possibilities (POS, word) for each one
    // It's crucial that we use zcat and process in parallel, lest we
    // have to store the entire 1GB file in memory.
    static bool mapped = false;
    static map<string, int> pos_tag_fixer;
    static map<int, string> pos_tag_gen;
    if (!mapped) {
        mapped = true;
        // BNC tags
        pos_tag_fixer["NOUN"] = NOUN_ID;
        pos_tag_fixer["SUBST"] = NOUN_ID;
        pos_tag_fixer["PUN"] = PUNCT_ID;
        pos_tag_fixer["ADV"] = ADV_ID;
        pos_tag_fixer["VERB"] = VERB_ID;
        pos_tag_fixer["CONJ"] = CONJ_ID;
        pos_tag_fixer["ADJ"] = ADJ_ID;
        pos_tag_fixer["ART"] = PRT_ID;
        pos_tag_fixer["INTERJ"] = X_ID;
        pos_tag_fixer["UNC"] = X_ID;
        pos_tag_fixer["PRON"] = PRON_ID;
        pos_tag_fixer["PREP"] = ADP_ID;

        // OANC tags. Not entirely certain that all map correctly.
        pos_tag_fixer["J"] = ADJ_ID;
        pos_tag_fixer["JJ"] = ADJ_ID;
        pos_tag_fixer["JJS"] = ADJ_ID;
        pos_tag_fixer["JJR"] = ADJ_ID;
        pos_tag_fixer["RB"] = ADJ_ID;
        pos_tag_fixer["RP"] = ADJ_ID;
        pos_tag_fixer["RBR"] = ADJ_ID;
        pos_tag_fixer["RBS"] = ADJ_ID;
        pos_tag_fixer["PDT"] = ADJ_ID;

        pos_tag_fixer["MD"] = ADV_ID;

        pos_tag_fixer["NN"] = NOUN_ID;
        pos_tag_fixer["NNS"] = NOUN_ID;
        pos_tag_fixer["NNP"] = NOUN_ID;
        pos_tag_fixer["NNPS"] = NOUN_ID;
        pos_tag_fixer["NNS|VBZ"] = NOUN_ID;
        pos_tag_fixer["NN|CD"] = NOUN_ID;
        pos_tag_fixer["NN|JJ"] = NOUN_ID;

        pos_tag_fixer["DT"] = PRT_ID;

        pos_tag_fixer["VB"] = VERB_ID;
        pos_tag_fixer["VBZ"] = VERB_ID;
        pos_tag_fixer["VBD"] = VERB_ID;
        pos_tag_fixer["VBN"] = VERB_ID;
        pos_tag_fixer["VBG"] = VERB_ID;
        pos_tag_fixer["VBP"] = VERB_ID;
        pos_tag_fixer["VBG|NN"] = VERB_ID;

        pos_tag_fixer["WP"] = DET_ID;
        pos_tag_fixer["WP$"] = DET_ID;
        pos_tag_fixer["WRB"] = DET_ID;
        pos_tag_fixer["WDT"] = DET_ID;
        pos_tag_fixer["EX"] = DET_ID;

        pos_tag_fixer["CD"] = NUM_ID;

        pos_tag_fixer["CC"] = CONJ_ID;

        pos_tag_fixer["PRP$"] = ADP_ID;
        pos_tag_fixer["PRP"] = ADP_ID;
        pos_tag_fixer["LS"] = ADP_ID;

        pos_tag_fixer["IN"] = ADP_ID;
        pos_tag_fixer["TO"] = PRT_ID;

        pos_tag_fixer["UH"] = X_ID;
        pos_tag_fixer["SYM"] = X_ID;
        pos_tag_fixer["FW"] = X_ID;
        pos_tag_fixer["POS"] = X_ID;

        pos_tag_fixer["$"] = PUNCT_ID;
        pos_tag_fixer[")"] = PUNCT_ID;
        pos_tag_fixer["("] = PUNCT_ID;
        pos_tag_fixer[";"] = PUNCT_ID;
        pos_tag_fixer[","] = PUNCT_ID;
        pos_tag_fixer[":"] = PUNCT_ID;
        pos_tag_fixer["."] = PUNCT_ID;
        pos_tag_fixer["#"] = PUNCT_ID;
        pos_tag_fixer["''"] = PUNCT_ID;
        pos_tag_fixer["``"] = PUNCT_ID;

        // Yes I know, array
        pos_tag_gen[10] = "_.";
        pos_tag_gen[11] = "_ADJ";
        pos_tag_gen[12] = "_ADP";
        pos_tag_gen[13] = "_ADV";
        pos_tag_gen[14] = "_CONJ";
        pos_tag_gen[15] = "_DET";
        pos_tag_gen[16] = "_NOUN";
        pos_tag_gen[17] = "_NUM";
        pos_tag_gen[18] = "_PRON";
        pos_tag_gen[19] = "_PRT";
        pos_tag_gen[20] = "_VERB";
        pos_tag_gen[21] = "_X";
    }

    shared_ptr<FILE> pipe(popen(("zcat -q " + path).data(), "r"), pclose);
    if (!pipe)
        return false;
    char buf[65536];
    size_t nwords = 0;
    size_t failcount = 0;
    while (fgets(buf, 65536, pipe.get()) && nwords < limit) {
        // We now have a new line in the buffer!
        size_t end = strlen(buf);
        if (end <= 3) {
            // No symbol + POS tag shorter than 3
            continue;
        }

        // Restructure buffer into a list of N words
        buf[end - 1] = '\0';
        int words = 1;
        for (size_t i = 0; i < end; i++) {
            if (buf[i] == ' ') {
                buf[i] = '\0';
                words++;
            }
        }

        int lastpos[4];
        int lastwrd[4];
        lastpos[3] = HEAD_ID;
        lastwrd[3] = HEAD_ID;
        char* feed = buf;
        int nc = 0;
        for (int i = 0; i < words; i++) {
            // Read in a string
            string s(feed);
            feed += s.size() + 1;

            size_t tailm = s.find_last_of('_');
            if (tailm == string::npos) {
                // Yikes! (Typically happens when
                // Abort, retry, fail....
                //                fprintf(stderr, "Yikes! |%s|\n", s.data());
                failcount++;
                continue;
            }

            string word = s.substr(0, tailm);
            string pos = s.substr(tailm + 1, s.size());
            if (pos_tag_fixer.find(pos) == pos_tag_fixer.end()) {
                fprintf(stderr, "Yikes! POS |%s| of word |%s|\n", pos.data(), s.data());
                failcount++;
                continue;
            }

            nwords++;

            int postype = pos_tag_fixer[pos];
            lastpos[0] = lastpos[1];
            lastpos[1] = lastpos[2];
            lastpos[2] = lastpos[3];
            lastpos[3] = postype;

            int id;
            auto cc = idLookup.find(word);
            if (cc == idLookup.end()) {
                id = idLookup.size() + N_RESERVED_IDS;
                idLookup[word] = id;
            }
            else {
                id = cc->second;
            }
            lastwrd[0] = lastwrd[1];
            lastwrd[1] = lastwrd[2];
            lastwrd[2] = lastwrd[3];
            lastwrd[3] = id;
            nc++;

            unigrams[tuple<int>(lastpos[3])]++;
            unigrams[tuple<int>(lastwrd[3])]++;

            unicounts[word + pos_tag_gen[lastpos[3]]]++;

            // Bigrams always work
            bigrams[tuple<int, int>(lastpos[2], lastpos[3])]++;
            bigrams[tuple<int, int>(lastwrd[2], lastpos[3])]++;
            bigrams[tuple<int, int>(lastpos[2], lastwrd[3])]++;
            bigrams[tuple<int, int>(lastwrd[2], lastwrd[3])]++;

            if (nc >= 3) {
                trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastpos[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastpos[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastwrd[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastwrd[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastpos[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastpos[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastwrd[3])]++;
                trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastwrd[3])]++;
            }
        }

        // Shift back again
        lastpos[0] = lastpos[1];
        lastpos[1] = lastpos[2];
        lastpos[2] = lastpos[3];
        lastpos[3] = TAIL_ID;
        lastwrd[0] = lastwrd[1];
        lastwrd[1] = lastwrd[2];
        lastwrd[2] = lastwrd[3];
        lastwrd[3] = TAIL_ID;
        // Bigrams always work
        bigrams[tuple<int, int>(lastpos[2], lastpos[3])]++;
        bigrams[tuple<int, int>(lastwrd[2], lastpos[3])]++;
        bigrams[tuple<int, int>(lastpos[2], lastwrd[3])]++;
        bigrams[tuple<int, int>(lastwrd[2], lastwrd[3])]++;

        if (nc >= 3) {
            trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastpos[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastpos[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastwrd[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastwrd[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastpos[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastpos[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastpos[2], lastwrd[3])]++;
            trigrams[tuple<int, int, int>(lastpos[1], lastwrd[2], lastwrd[3])]++;
        }
    }
    printf("Loaded %lu words (goofed %lu)\n", nwords, failcount);

    return true;
}

static PyObject* ngrams_load_ngrams(PyObject* self, PyObject* args)
{
    bool ok;
    string source = castToString(args, &ok);
    if (!ok) {
        PyErr_SetString(PyExc_RuntimeError, "Expected a pure ascii string as the keyword.");
        return NULL;
    }
    const char* gv = getenv("GRAVEYARD");
    if (source == "coha" || source == "coca") {
        if (!gv) {
            PyErr_SetString(PyExc_RuntimeError, "The GRAVEYARD environment variable must be set.");
            return NULL;
        }
        if (!load_ngram_table(gv + string("/") + source + string("1.gz")))
            return nullptr;
        if (!load_ngram_table(gv + string("/") + source + string("2.gz")))
            return nullptr;
        if (!load_ngram_table(gv + string("/") + source + string("3.gz")))
            return nullptr;
        if (!load_ngram_table(gv + string("/") + source + string("4.gz")))
            return nullptr;
        printf("Loaded: %lu %lu %lu %lu\n", unigrams.size(), bigrams.size(), trigrams.size(), quatrigrams.size());
        return Py_None;
    }
    else if (source == "bncsmall") {
        if (!load_tagged_corpus(gv + string("/tagbnc.gz"), 1000000)) {
            return nullptr;
        }
        printf("Loaded: %lu %lu %lu %lu %lu\n", unicounts.size(), unigrams.size(), bigrams.size(), trigrams.size(), quatrigrams.size());
        return Py_None;
    }
    else if (source == "bnc") {
        if (!load_tagged_corpus(gv + string("/tagbnc.gz"), -1)) {
            return nullptr;
        }
        printf("Loaded: %lu %lu %lu %lu %lu\n", unicounts.size(), unigrams.size(), bigrams.size(), trigrams.size(), quatrigrams.size());
        return Py_None;
    }
    else if (source == "oanc") {
        if (!load_tagged_corpus(gv + string("/tagoanc.gz"), -1)) {
            return nullptr;
        }
        printf("Loaded: %lu %lu %lu %lu %lu\n", unicounts.size(), unigrams.size(), bigrams.size(), trigrams.size(), quatrigrams.size());
        return Py_None;
    }
    else {
        PyErr_SetString(PyExc_RuntimeError, "Could not load that source.");
        return nullptr;
    }
}

static PyObject* ngrams_query_ngram(PyObject* self, PyObject* args)
{
    switch (PyTuple_Size(args)) {
    case 1: {
        auto cc = unigrams.find(tuple<int>(PyLong_AsLong(PyTuple_GetItem(args, 0))));
        return PyLong_FromLong(cc == unigrams.end() ? 0 : cc->second);
    } break;
    case 2: {
        auto cc = bigrams.find(tuple<int, int>(PyLong_AsLong(PyTuple_GetItem(args, 0)),
            PyLong_AsLong(PyTuple_GetItem(args, 1))));
        return PyLong_FromLong(cc == bigrams.end() ? 0 : cc->second);
    } break;
    case 3: {
        auto cc = trigrams.find(tuple<int, int, int>(PyLong_AsLong(PyTuple_GetItem(args, 0)),
            PyLong_AsLong(PyTuple_GetItem(args, 1)),
            PyLong_AsLong(PyTuple_GetItem(args, 2))));
        return PyLong_FromLong(cc == trigrams.end() ? 0 : cc->second);
    } break;
    case 4: {
        auto cc = quatrigrams.find(tuple<int, int, int, int>(PyLong_AsLong(PyTuple_GetItem(args, 0)),
            PyLong_AsLong(PyTuple_GetItem(args, 1)),
            PyLong_AsLong(PyTuple_GetItem(args, 2)),
            PyLong_AsLong(PyTuple_GetItem(args, 3))));
        return PyLong_FromLong(cc == quatrigrams.end() ? 0 : cc->second);
    } break;
    default:
        return PyLong_FromLong(0);
    }
}

static PyObject* ngrams_lookup_word(PyObject* self, PyObject* args)
{
    bool ok;
    string word = castToString(args, &ok);
    if (!ok) {
        PyErr_SetString(PyExc_RuntimeError, "Expected a pure ascii string.");
        return NULL;
    }
    auto cc = idLookup.find(word);
    return PyLong_FromLong(cc == idLookup.end() ? -1 : cc->second);
}

static PyObject* ngrams_load_options(PyObject* self, PyObject* args)
{
    PyObject* dict = PyDict_New();
    const char* opts[][2] = {
        { "coha", "Corpus of Historical American English (520M words), all ngrams" },
        { "coca", "Corpus of Contemporary American English (440M words), top 1M ngrams" },
        { "bncsmall", "British National Corpus (First 1M words). Also loads POS DB" },
        { "bnc", "British National Corpus (100M words). Also loads POS DB" },
        { "oanc", "Open American National Corpus (15M words). Also loads POS DB" },
        { NULL, NULL }
    };
    for (int i = 0; opts[i][0]; i++) {
        PyDict_SetItem(dict, PyUnicode_FromString(opts[i][0]), PyUnicode_FromString(opts[i][1]));
    }
    return dict;
}

static PyObject* ngrams_list_special(PyObject* self, PyObject* args)
{
    PyObject* dict = PyDict_New();
    for (int i = 0; SPECIAL_TYPES[i].desc; i++) {
        PyObject* desc = PyUnicode_FromString(SPECIAL_TYPES[i].desc);
        PyObject* key = PyLong_FromLong(SPECIAL_TYPES[i].key);
        PyDict_SetItem(dict, desc, key);
    }
    return dict;
}

static PyMethodDef NGramsMethods[]
    = {
        /* misc api */
        { "system", ngrams_system, METH_VARARGS,
            "Execute a shell command." },
        { "random", ngrams_random, METH_NOARGS,
            "Generate a random float in [0..1]." },

        /* part of speech handling api */
        { "pos_load", ngrams_load_pos, METH_O,
            "Takes a filename. Load the part of speech table. Only does something if table is empty." },
        { "pos_query", ngrams_query_count, METH_O,
            "Get the number of occurences in corpus for a tagged part of speech" },
        { "pos_list_categories", ngrams_list_pos_categories, METH_NOARGS,
            "Return a list containing the possible parts of speech" },

        { "ng_load", ngrams_load_ngrams, METH_O,
            "Takes an ascii string code, and loads a corresponding ngrams source" },
        { "ng_query", ngrams_query_ngram, METH_O,
            "Takes a k-tuple of integral ids and returns the number of times it is found in corpus." },
        { "ng_lookup", ngrams_lookup_word, METH_O,
            "Takes an ascii word and returns a unique numeric id, or -1 if no match." },
        { "ng_list_special", ngrams_list_special, METH_NOARGS,
            "Return a dictionary mapping token types to numeric ids." },
        { "ng_load_options", ngrams_load_options, METH_NOARGS,
            "Return a dictionary mapping containing keywords and descriptions of ngram sources." },

        { "rate", ngrams_rate, METH_O,
            "Assign a score to a sentence (list of bytes() objects)." },

        { NULL, NULL, 0, NULL } /* Sentinel */
      };

static const char* ngrams_doc = "For efficient handling of ngrams";

static struct PyModuleDef ngramsmodule = {
    PyModuleDef_HEAD_INIT,
    "ngrams", /* name of module */
    ngrams_doc, /* module documentation, may be NULL */
    -1, /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
    NGramsMethods
};

extern "C" {

PyMODINIT_FUNC
PyInit_ngrams(void)
{
    return PyModule_Create(&ngramsmodule);
}
}
