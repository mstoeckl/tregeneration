#!/usr/bin/env python3

"""

Main code file. This file handles reading ta.input/ta.output, as well as
most of the administrative stuff.

"""

#
# Import helpful modules
#
import os, sys, subprocess
import time
import random
from math import *

#
# Build and load the ngram code, if it hasn't been done already
#
scriptloc = os.path.dirname(os.path.abspath(__file__))
if os.getuid() == os.stat(scriptloc).st_uid:
    # Only owner can build
    oldloc = os.path.abspath(os.getcwd())
    os.chdir(scriptloc)
    try:
        z = subprocess.check_output(sys.executable + " setup.py build",shell=True)
    except subprocess.CalledProcessError:
        print("build failed")
        quit()
    if len(z.split()) != 4:
        print(z.decode("utf8"),file=sys.stderr)
    subprocess.check_output("ln -sf build/lib*/ngrams* ngrams.so",shell=True)
    os.chdir(oldloc)
import ngrams
print("ngrams library ready")

#
# Get location for datasets, or die trying
#
if "GRAVEYARD" in os.environ:
    GRAVEYARD = os.environ["GRAVEYARD"] + '/'
else:
    print("In order to find the various datasets,\nyou must first set the GRAVEYARD environment variable.",file=sys.stderr)
    quit()

#
# Define helper functions/shims for the ngram backend that will be useful later
#

# Unless anything changes...
#['.', 'ADJ', 'ADP', 'ADV', 'CONJ', 'DET', 'NOUN', 'NUM', 'PRON', 'PRT', 'VERB', 'X']
parts_of_speech = ngrams.pos_list_categories()
special_tokens = ngrams.ng_list_special()

def baseform(word):
    # We strip $/# signs because they are so rare.
    return word.strip("\"',.;:?!$#")

def posfreq(word):
    """
    Return a dictionary indicating the likely parts of speech (and counts)
    of the base form of a word.
    """
    word = baseform(word)
    z = {}
    for cat in parts_of_speech:
        z[cat] = ngrams.pos_query(word+"_"+cat)
    z[None] = ngrams.pos_query(word)
    return z

def most_likely_tags(sentence):
    """
    Generate the most likely tags for a sentence, according to the current
    parts of speech list
    """
    if isinstance(sentence, list):
        sent = sentence
    elif isinstance(sentence, str):
        sent = list(filter(None, sentence.split(" ")))
    result = []
    for word in sent:
        p = posfreq(word)
        netcounts = sum(x[1] for x in p.items() if x[0] is not None)
        if netcounts == 0:
            # Don't know, guessing NOUN
            result.append((word, "NOUN"))
            continue
        # Select a tag at random in accordance with the frequencies
        target = random.randint(0,netcounts-1)
        for b,c in p.items():
            if b is None:
                continue
            if target < c:
                result.append((word, b))
                break
            target -= c
    return result

def combo_freq(*words):
    z = tuple(ngrams.ng_lookup(baseform(w)) for w in words)
    return ngrams.ng_query(z)

#
# Code that gets run whenever script is read; the __name__ == __main__
# skips running this code, when you, e.g., do "from main import *" in a REPL
#
if __name__ == "__main__":
    # Only run when used as a script, not as a library

    before = time.time()
    if False:
        # Load expensive POS list (words of count 40+)
        #ngrams.pos_load(GRAVEYARD + "/google-unigrams.gz")
        # Load cheaper POS list (words of count 1K+)
        ngrams.pos_load(GRAVEYARD + "/google-unigrams-1K.gz")
        after = time.time()
        print("Loading POS took {:.3f} seconds".format(after-before))

        before = time.time()
        ngrams.ng_load("coha")
        after = time.time()
        print("Loading NGRAMS took {:.3f} seconds".format(after-before))
    else:
        #ngrams.ng_load("bncsmall")
        print("Loading TAGGED CORPUS may take a while.")
        #ngrams.ng_load("bnc")
        ngrams.ng_load("oanc")
        after = time.time()
        print("Loading TAGGED CORPUS took {:.3f} seconds".format(after-before))

    # Load ta.input
    target = GRAVEYARD + "../ta.input"
    lines = [z.split() for z in open(target).read().split('=====') if z != '\n']

    #
    # Example use. Tagging all the words by their probablistically most 
    # likely known part of speech.
    #
    for line in lines:
        tags = most_likely_tags(line)
        newsent = " ".join(t[0]+'—'+t[1].lower() for t in tags)
        print(newsent)

    #
    # Example use: Likelihoods
    #
    sentences = ["The quick brown fox jumped over the lazy dog.".split(),
                 "All is well that ends well.".split(),
                 "Isn't that nice.".split()]
    for sentence in sentences:
        for s1 in sentence:
            print(s1,combo_freq(s1))
        for s1,s2 in zip(sentence, sentence[1:]):
            print(s1,s2,combo_freq(s1,s2))
        for s1,s2,s3 in zip(sentence, sentence[1:],sentence[2:]):
            print(s1,s2,s3,combo_freq(s1,s2,s3))
        for s1,s2,s3,s4 in zip(sentence, sentence[1:],sentence[2:],sentence[3:]):
            print(s1,s2,s3,s4,combo_freq(s1,s2,s3,s4))
