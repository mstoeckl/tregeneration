#!/usr/bin/env python3

from distutils.core import setup, Extension

mod = Extension('ngrams',
                sources = ['ngrams.cpp'],
                extra_compile_args=['-std=c++11','-Os'],
                extra_link_args=[],
                libraries=[])

setup (name = 'NGrams',
       version = '0.0',
       description = 'Module for efficient ngram work.',
       ext_modules = [mod])
