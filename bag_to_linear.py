#!/usr/bin/env python3

import time, os
from pprint import pprint
from random import shuffle, choice, randint
from math import factorial
from collections import defaultdict
from statistics import mean, stdev

pos = {'.', 'ADJ', 'ADP', 'ADV', 'CONJ', 'DET', 'NOUN', 'NUM', 'PRON', 'PRT', 'VERB', 'X'}

# The fire cat chased the fire dog.
gold_standard = "The fire cat chased the fire dog."

#gold_standard = "Where did all the birds go?"

#gold_standard = "Roe is actually fish eggs."

#gold_standard = "Never do I look to the sky before it falls."
#gold_standard = "I ate with a fork."
gold_standard = "Metonymy is unambiguous unless preventable."


words = gold_standard.split()
words = sorted(words)

actual = (("The_DET",("fire_ADJ","cat_NOUN")),"chased_VERB",("the_DET",("fire_ADJ","dog._NOUN")))

#posinfo = {
    #"The":[('DET',2762130944),('NOUN',1008290)],
    #"cat":[('NOUN',8543522)],
    #"chased":[('VERB',1424869),('ADJ',13147)],
    #"the":[('DET',23682114891),('ADJ',6288527),('NOUN',11074)],
    #"fire":[('NOUN',44546474),('VERB',2519770),('ADJ',2837830)],
    #"dog.":[('NOUN',16654723),('ADJ',1103048)],
    
    #"Where":[("ADV",31264007)],
    #"all":[("DET",918534350),("ADJ",10583527),("ADV",1563566),("NOUN",52716)],
    #"birds":[("NOUN",17186793)],
    #"did":[("VERB",328985185)],
    #"go?":[("VERB",149839529),("ADJ",1363566),("NOUN",716211)]
    #}
    
import ngrams
ngrams.pos_load(os.environ["GRAVEYARD"] + "/google-unigrams-10K.gz")

def baseform(word):
    return word.strip("\"',.;:?!$#")

posinfo = {}
for word in words:
    wd = baseform(word)
    cc = []
    for pos in ngrams.pos_list_categories():
        z =  ngrams.pos_query(wd + "_" + pos) 
        if z> 0:
            cc.append((pos, z))
    posinfo[word] = cc
print(posinfo)

seeds = [('NOUN','VERB'),('NOUN','VERB','NOUN'),('NOUN','VERB','ADJ'),('X')]

# Crucially, no ordering assumptions. One word is _always_ adopted.
rewrite_rules = {
    # Future: Include multi-segment rewrite rules. (And ones based on intermediate tags)
    
    "NOUN":[("ADJ","NOUN"),("DET","NOUN")],
    "VERB":[("ADV","VERB")],

    # Do _with_ sthg
    "VERB":[("VERB","ADP","NOUN")],
    "VERB":[("VERB","ADP","ADJ")]
    # ? ADP: Sorting the trees from 
    }

def removed(li, obj):
    if isinstance(li, tuple):
        z = list(li).copy()
        z.remove(obj)
        return tuple(z)
    else:
        z = li.copy()
        z.remove(obj)
        return z

def can_get(group, pool):
    # Arg, constraint satisfaction.
    for item in group:
        q = len(pool)
        for word in pool:
            meanings = posinfo[word]
            mc = [p[1] for p in meanings if p[0] == item]
            if mc:
                pool = removed(pool, word)
                break
        if len(pool) == q:
            return False
    return True

def recursive_pos_tuples(tree):
    if isinstance(tree, str):
        return tree.split("_")[-1]

    z = []
    rest = []
    for elem in tree:
        q = recursive_pos_tuples(elem)
        if not isinstance(q, str):
            rest += q[1:]
            z.append(q[0])
        else:
            z.append(q)
            rest.append(q)

    return (tuple(z), *rest)

def tree_count_targets(tree, target):
    if isinstance(tree, str):
        if tree.split("_")[-1] == target:
            return 1
        else:
            return 0
    count = 0
    for t in tree:
        count += tree_count_targets(t, target)
    return count
        

def tree_pos_extend(tree, target, conv_extra):
    """
    Given: Pos-tag. In retrospect, perhaps two parallel trees would have been better.
    """
    if isinstance(tree, str):
        if tree.split("_")[-1] == target:
            return [tree] + conv_extra
        else:
            return tree
    # Only replace one, at random
    count = tree_count_targets(tree, target)
    moddt = []   
    #print(count)
    
    selected = randint(0,count - 1)
    count = 0
    for t in tree:
        exp = tree_count_targets(t, target)
        if exp == 0:
            moddt.append(t)
            continue
        
        if count >= selected and count < selected + exp:
            #
            ext = tree_pos_extend(t, target, conv_extra)
            moddt.append(ext)
        else:
            moddt.append(t)
            
        count += exp
        
        #repl = tree_pos_extend(t, target, conv_extra)
        #if repl != t:
            #if count == selected:
                #moddt.append(repl)
            #else:
                #moddt.append(t)
            #count += 1
        #else:
            #moddt.append(t)
    return moddt

class Tagged():
    def __init__(self,entity,pos):
        self.entity = entity
        self.pos = pos
    def __repr__(self):
        return "<"+str(pos)+":"+repr(entity)+">"

def make():
    pool = words
    opts = [s for s in seeds if can_get(s, pool)]
    seed = choice(opts)
    # Build the structure out of a tree to mutate it...
    tree = []
    for pos in seed:
        opts = [wrd for wrd in pool if pos in (q[0] for q in posinfo[wrd])]
        if not opts:
            # We have contention; give up.
            return None
        word = choice(opts)
        tree.append(word + "_" + pos)
        pool = removed(pool, word)

    #print("Start:", tree,"\n")
    while pool:
        # Construct a list of options. Match on _type_, then re-seek location
        structures = recursive_pos_tuples(tree)
        opts = []
        for struct in structures:
            if struct not in rewrite_rules:
                continue
            replacements  = rewrite_rules[struct]
            for r in replacements:
                extra = removed(r,struct)
                if can_get(extra, pool):
                    opts.append((struct, r))

        if not opts:
            return None
        target = choice(opts)
        extra = removed(target[1], target[0])
        conv_extra = []
        for pos in extra:
            opts = [wrd for wrd in pool if pos in (q[0] for q in posinfo[wrd])]
            word = choice(opts)
            conv_extra.append(word + "_" + pos)
            pool = removed(pool, word)

        # Now, only need to append conv_extra to the rest.
        # Remember sort invariant!
        #print("postree",tree)
        #print("target", target[0], "extra", conv_extra)
        # > warning, greedy
        #print(tree, target[0], conv_extra)
        tree = tree_pos_extend(tree, target[0], conv_extra)
        #print("now have:",tree,pool)
        #print()

    return tree

def tree_sorted(tree):
    """
    Put unordered tree in a normal form
    """
    if isinstance(tree, tuple) or isinstance(tree, list):
        return tuple(sorted([tree_sorted(t) for t in tree], key=lambda x:str(x)))
    else:
        return tree

def linearize(tagged_tree):
    # Recursive shuffle. Crude, but could be worse
    if isinstance(tagged_tree, str):
        return "_".join(tagged_tree.split("_")[:-1])
    
    tt = list(tagged_tree)
    shuffle(tt)
    return " ".join([linearize(t) for t in tt])

found = defaultdict(int)
pertree = []
total = 0
trees = defaultdict(int)
prevt = -3
try:
    for i in range(10000):
        if i % 100 == 0:
            print(i, "trees:",len(trees), "linear:", len(found))
            if len(trees) <= prevt:
                break
            prevt = len(trees)

        while True:
            r = make()
            if r is not None:
                break

        r = tree_sorted(r)
        if str(r) in trees:
            continue
        trees[str(r)] = r
        print(str(r))
        #print(r)
            
        sfound = defaultdict(int)
        tries = 300
        # ^ So, only about 96 options/tree
        for i in range(tries):
            sfound[linearize(r)] += 1
            total += 1
        pertree.append(len(sfound))
        for s,v in sfound.items():
            found[s] += v
except KeyboardInterrupt:
    pass
            
print("ntrees:",len(trees),len(words)**(len(words)-2))
if pertree:
    print("pertree:",mean(pertree),"+/-",stdev(pertree))
print("sentences:",len(found),"of",factorial(len(words)))
if gold_standard in found:
    print(gold_standard, found[gold_standard], total)
q = tree_sorted(actual)
if q in trees:
    print("Found: ", q)
with open("log","w") as f:
    for k,v in found.items():
        print(k,file=f)

#pprint(r)
#pprint(linearize(r))

# When we linearize, we do so recursively & at random


# Make heavy use of sorting for identity 
    
    
# Strategy: Enumerate _all_ possibilities, then pick one.
